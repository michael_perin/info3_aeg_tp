Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes, INP / Polytech Grenoble

Part of the course **"Parsers which Compute"**

# 2h practise: A CALCULATOR for simple arithmetic expressions

## In the `calc/` directory

### 1. complete the file `Parser.jj`

* to be able to parse INTEGERS
* test your parser in a terminal, using `java -cp ../../../../../../bin info3.parser.javacc.tp.calc.Parser "-123"`

### 2. complete your parser

* to be able to compute expressions "1+2-3"
* test your parser in a terminal

### 3. complete your parser 

* to be able to compute expressions "1+2-(3*4)"
* test your parser in a terminal

### 4. modify your parser to take into account operator priority

* to be able to compute expressions "3 + 4*2 - 34 mod 3*2"
* test your parser in a terminal
