Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes, INP / Polytech Grenoble

Part of the course **"Parsers which Compute"**

# 1h practise: Recognizing languages on the {a,b} alphabet

## in the `ab/` directory

### Parser of the language {a^n.b^n | n ∈ Nat}

We consider the grammar

```haskell
P ::= S . <EOF>
S ::=
    | a . S . b
    | ""
```
* complete the file `Parser_anbn.jj`

* test your parser in a terminal, using `java -cp ../../../../../bin info3.parser.javacc.tp.ab.Parser_anbn abba`


### Parser of the language { w ∈ {a,A,b,B}* | #{a,A}(w) = #{b,B}(w) }

We consider the grammar below which accepts any words on {a,A,b,B}.

```haskell
P ::= S . <EOF>
S ::=
    | a . S
    | b . S
    | ""
```


* complete the file `Parser.jj` and `Couple.java`

* test your parser in a terminal, using `java -cp ../../../../../bin info3.parser.javacc.tp.ab.Parser aBabBbaabAAb`
