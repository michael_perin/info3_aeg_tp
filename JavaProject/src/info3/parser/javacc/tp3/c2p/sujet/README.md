Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes, INP / Polytech Grenoble

Part of the course **"Parsers which Compute"**

# 2h practise: A CALCULATOR for simple arithmetic expressions

## In the `tp3/c2p/` directory

The goal is to developp a translator of variables declarations from the `C` programming langage to the `Pascal` programming language.

```C 
int x, y = 0;
bool b1, b2 = true;
int var;
```

is translated into

```Pascal
var x : integer = 0;
var y : integer = 0;
var b1 : boolean = true;
var b2 : boolean = true;
var warning_var : int;
```

### 1. learn to debug a parser
- compile the file `Parser.jj`
- run it on the example : using `make parse` in the `test/` directory

### 2. correct the bugs
 