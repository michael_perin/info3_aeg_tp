Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes, INP / Polytech Grenoble

Part of the course **"Parsers which Compute"**

# 1h practise: Recognizing languages on the {a,b} alphabet

## in the `tp1/ab/` directory

### Parser of the language {a^n.b^n | n ∈ Nat}

We consider the grammar

```haskell
P ::= S . <EOF>
S ::=
    | a . S . b
    | ε
```
* complete the file `Parser_anbn.jj`

* test your parser in a terminal, in the directory `bin/` of your JavaProject, using `java -cp . info3.parser.javacc.tp1.ab.Parser_anbn aaabbb`


### Parser of the language { w ∈ {a,A,b,B}* | #{a,A}(w) = #{b,B}(w) }

We consider the grammar below which accepts any words on {a,A,b,B}.

```haskell
P ::= S . <EOF>
S ::=
    | (a|A) . S
    | (b|B) . S
    | ε
```


* complete the file `Parser.jj` and `Couple.java`

* test your parser in a terminal, in the directory `bin/` of your workspace
 using `java -cp . info3.parser.javacc.tp1.ab.Parser aBabBbaabAAb`
