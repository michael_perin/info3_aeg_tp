/* Michaël PÉRIN,
 * VERIMAG / Univ. Grenoble-Alpes
 *  INP / Polytech Grenoble
 */

// A SIMPLE PARSER which
// - recognizes words of the language { a^n.b^n | n ∈ Nat }
// - computes n

// USAGE: in the directory `bin/` of your workspace 
// java -cp . info3.parser.javacc.tp1.ab.Parser_anbn aaabbb

PARSER_BEGIN(Parser_anbn)

package info3.parser.javacc.tp1.ab;

import java.io.BufferedReader;
import java.io.FileReader;
import info3.shared.Tracing;

public class Parser_anbn {

  public static int parse_file(String path_file) throws Exception {
    return new Parser_anbn(new BufferedReader(new FileReader(path_file))).parse();
  }

	public static int parse_string(String input_string) throws Exception {
	  return new Parser_anbn(new java.io.StringReader(input_string)).parse();
  }

  public static void main(String[] args) throws Exception {
    Tracing.enable_tracing(true);
    Tracing.enable_xterm_font(true);
    //
    String input = args[0];
    //
    String parser_name = "Parser { a^n.b^n | n ∈ Nat }";
    int n = -1;
    Tracing.call(0, parser_name);
		n = Parser_anbn.parse_string(input);
		if (n >= 0)
			Tracing.parsed(0, parser_name, String.format("succeeds with n = %d\n",n));
		else
			Tracing.failed(0, parser_name, "fails\n");
  }
}
PARSER_END(Parser_anbn)


// == LEXER

SKIP : { " " }

TOKEN : { "a" | "b" }


// == PARSER of the langage { a^n.b^n | n in Nat }

//  parse ::= S . EOF
//  S ::=
//      | "a" . S . "b"
//      | ε

// The parser with computations of n

int parse() :
{ int n; }
{
  n = S(0)
  <EOF>
    { return n; }
}


int S(int t) : // The parameter t is only used for tracing
{ 
  Tracing.call(t,"S");
  int n; 
}
{
  A(t+1)
  n = S(t+1)
  B(t+1)
  { 
    Tracing.returns(t, "S", String.format("%d",/*...*/));
    /*...........*/ 
  }

| Epsilon(t+1)
  { 
    Tracing.returns(t, "S", /*...*/);
    /*.........*/ 
  }
}


void A(int t) :
{ 
  Tracing.call(t,"A");
}
{
  "a" { Tracing.traceinGreen("a"); }
}


void B(int t) :
{ 
  Tracing.call(t,"B");
}
{
  "b" { Tracing.traceinYellow("b"); }
}


void Epsilon(int t) :
{ 
  Tracing.call(t,"Epsilon");
}
{
  /*epsilon*/ { Tracing.traceinGray("ε"); }
}
