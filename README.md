> Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes / INP / Polytech Grenoble

# "Parsers which Compute"

Course given at Polytech Grenoble Computer Science Engineering School
by Michaël PÉRIN (michael.perin@univ-grenoble-alpes.fr).

#### Course

* The slides of the course are available in [.pdf](http://www-verimag.imag.fr/~perin/enseignement/RICM3/infaeg/cours/pdf/INFO3_A&G_PARSER.pdf)

#### Developping parsers in JavaCC

* 4h Practise
* 6h Project

## Part 1 - 30min Installation of JavaCC

#### Clone this git repository

> git clone git@gricad-gitlab.univ-grenoble-alpes.fr:michael_perin/info3_aeg.git

#### Import files in Eclipse

Close Eclipse and relauch it with **Workspace=** *the directory of your git clone*

#### Install the JavaCC plugin for Eclipse

  1. Lauch Eclipse
  2. Eclipse: Preferences -> General -> Install/update -> Available software sites

      - add `http://eclipse-javacc.sourceforge.net/`

  3. Eclipse: Help -> Install new software -> work with : select JavaCC

      - check [x] JavaCC Eclipse Plug-in
      - ( [Next] )* ; [finish]
      - At some point, a popup should appear, asking you to trust the sourceforge provided.

  4. Restart Eclipse with the appropriate workspace
  5. Eclipse: Window -> show view -> Package Explorer
  6. Package Explorer: `src/` -> buildPath -> Configure Build Path -> JavaCC options -> javacc options : desactivate STATIC [Apply and Close]

* In case of trouble
  - [Installation guide](http://eclipse-javacc.sourceforge.net)
  - [JavaCC official website](https://javacc.org)


## Part 2 - 6h Practise

  *Small but tricky parsers that illustrate the most common problems.*

#### [Practise 1](JavaProject/src/info3/parser/javacc/tp1/ab/sujet/README.md) (1h30)
#### [Practise 2](JavaProject/src/info3/parser/javacc/tp2/calc/sujet/README.md) (2h)
#### [Practise 3](JavaProject/src/info3/parser/javacc/tp3/c2p/sujet/README.md) (2h)

## Part 3 - 6h Project
